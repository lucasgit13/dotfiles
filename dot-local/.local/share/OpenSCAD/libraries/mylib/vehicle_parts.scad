module wheel(wheel_radius=10,wheel_width=4,hole_radius=2){
    difference(){
    sphere(wheel_radius);
        translate([0,(-wheel_width/2)-wheel_radius,0])
            cube(wheel_radius*2,center=true);
        translate([0,(wheel_width/2)+wheel_radius,0])
            cube(wheel_radius*2,center=true);

        translate([0,0,wheel_radius/2])
        rotate([90,0,0])
        cylinder(h=wheel_width+1,r=hole_radius,center=true);
            
        translate([wheel_radius/2,0,0])
        rotate([90,0,0])
        cylinder(h=wheel_width+1,r=hole_radius,center=true);
            
        translate([0,0,-wheel_radius/2])
        rotate([90,0,0])
        cylinder(h=wheel_width+1,r=hole_radius,center=true);
            
        translate([-wheel_radius/2,0,0])
        rotate([90,0,0])
        cylinder(h=wheel_width+1,r=hole_radius,center=true);
        }
}
//--
module simple_wheel(wheel_radius=10,wheel_width=4){
    difference(){
    sphere(wheel_radius);
        translate([0,(-wheel_width/2)-wheel_radius,0])
            cube(wheel_radius*2,center=true);
        translate([0,(wheel_width/2)+wheel_radius,0])
            cube(wheel_radius*2,center=true);   
        }
}
//--
module complex_wheel(wheel_radius=10, side_spheres_radius=50, hub_thickness=4, cylinder_radius=2) {
    cylinder_height=2*wheel_radius; 
    difference() { 
        // Wheel sphere
        sphere(r=wheel_radius); 
        // Side sphere 1 
        translate([0,side_spheres_radius + hub_thickness/2,0])
            sphere(r=side_spheres_radius); 
        // Side sphere 2 
        translate([0,- (side_spheres_radius + hub_thickness/2),0])
            sphere(r=side_spheres_radius); 
        // Cylinder 1
        translate([wheel_radius/2,0,0])
            rotate([90,0,0])
            cylinder(h=cylinder_height,r=cylinder_radius,center=true); 
        // Cylinder 2 
        translate([0,0,wheel_radius/2])
            rotate([90,0,0])
            cylinder(h=cylinder_height,r=cylinder_radius,center=true); 
        // Cylinder 3 
        translate([-wheel_radius/2,0,0])
            rotate([90,0,0])
            cylinder(h=cylinder_height,r=cylinder_radius,center=true); 
        // Cylinder 4 
        translate([0,0,-wheel_radius/2])
            rotate([90,0,0])
            cylinder(h=cylinder_height,r=cylinder_radius,center=true); 
    }
}
//--
module spoke_wheel(radius=12,width=5,spokes=7,spoke_radius=1.5,thickness=4){
    //variables
        spoke_length=radius-(thickness/4);
        step=360/spokes;
union(){
    for (i=[0:step:359]){
        angle=i;
        rotate([0,angle,0]) cylinder(r=spoke_radius,h=spoke_length);
    }

    difference(){
        rotate([90,0,0]) cylinder(r=radius,h=width,center=true);
        rotate([90,0,0]) cylinder(r=radius-thickness,h=width+1,center=true);
    }
}
}
//--
module rounded_simple_wheel(wheel_radius=20,wheel_width=6,tyre_diameter=4,axle_radius=3){
difference(){
    rotate([90,0,0]){
    rotate_extrude(angle=360){
        translate([wheel_radius - tyre_diameter/2, 0])
            circle(d=tyre_diameter);
        translate([0,-wheel_width/2])
            square([wheel_radius-tyre_diameter/2,wheel_width]);  
}
    }
    rotate([90,0,0])
    cylinder(h=wheel_width+1,r=axle_radius,center=true);
}
    }
//--
module body(base_height=8,top_height=10,width=20,base_length=60,top_length=30,top_offset=5,body_roll=0,top=true,front_bumper=false,rear_bumper=false){
    rotate([body_roll,0,0]) {
        
        // Car body base
        cube([base_length,width,base_height],center=true);
        // Car body top
        if(top){
        translate([top_offset,0,base_height/2+top_height/2 - 0.001])
        cube([top_length,width,top_height],center=true); 
          }
          
         // Rear bumper
          if(rear_bumper){
    color("blue") {
        translate([base_length/2,0,0])rotate([90,0,0]) {
            cylinder(h=width - base_height,r=base_height/2,center=true);
            translate([0,0,(width - base_height)/2])
                sphere(r=base_height/2);
            translate([0,0,-(width - base_height)/2])
                sphere(r=base_height/2);
            }
        }
    }
         // Front bumper
    if(front_bumper){
        color("blue") {
        translate([-base_length/2,0,0])rotate([90,0,0]) {
            cylinder(h=width - base_height,r=base_height/2,center=true);
            translate([0,0,(width - base_height)/2])
                sphere(r=base_height/2);
            translate([0,0,-(width - base_height)/2])
                sphere(r=base_height/2);
            }
        }
    }
    }
}
//--
module extruded_car_body(length=80,rear_height=20,rear_width=25,scaling_factor=0.5,rounded=false){
rotate([0,-90,0])
    linear_extrude(height=length,scale=scaling_factor,center=true){
resize([rear_height,rear_width])
    circle(d=rear_height);
    }
        if(rounded){
        //rear rounded
        translate([length/2,0])
            resize([rear_height,rear_width,0])
                sphere(d=rear_height);
        //front rounded
        translate([-length/2,0])
            scale(scaling_factor)
            resize([rear_height,rear_width,0])
                sphere(d=rear_height);
        }
}
//--
module axle(axle_radius=2,track=30){
rotate([90,0,0]) 
cylinder(h=track,r=axle_radius,center=true);
}
//--
module axle_wheelset(track=35,axle_radius=2) {
    translate([0,track/2,0])
        children(0);
    axle(track=track,axle_radius);
    translate([0,-track/2,0])
        children(0);
}
//--
module naca_airfoil(t,n,chord){
    function naca_half_thickness(x,t)=5*t*(0.2969*sqrt(x)-0.1260*x-0.3516*pow(x,2)+0.2843*pow(x,3)-0.1015*pow(x,4));

    function naca_top_coordinates(t,n)=[for(x=[0:1/(n-1):1]) [x,naca_half_thickness(x,t)]];
       
    function naca_bottom_coordinates(t,n) = [ for (x=[1:-1/(n-1):0]) [x, - naca_half_thickness(x,t)]];
        
    function naca_coordinates(t,n) = concat(naca_top_coordinates(t,n), naca_bottom_coordinates(t,n));

    points = naca_coordinates(t,n);
    scale([chord,chord,1])
    polygon(points);
}
//--
module naca_wing(t,n,chord,span,center=false){
        linear_extrude(height=span,center=center){
        naca_airfoil(t,n,chord);
        }
    }
//--    
module spoiler(scale=[1,1,1]){ 
        distance=10;
        vspun=15;
        vchord=15; 
scale(scale){
    translate([0,-2,vspun/2]) rotate([90,0,90]) naca_wing(0.12,500,20,50,center=true);
        
    translate([-distance,0,0]) rotate([0,0,90]) naca_wing(0.12,500,vchord,vspun,center=true);
        
     translate([distance,0,0]) rotate([0,0,90]) naca_wing(0.12,500,vchord,vspun,center=true);
}
}    
//--
    
    