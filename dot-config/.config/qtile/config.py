import os
import re
import socket
import subprocess
from libqtile import qtile
from typing import List  # noqa: F401
from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, KeyChord, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

mod = "mod4"
alt = "mod1"
myTerm = "st"
myBrowser = "brave"

keys = [
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    # Key([mod], "apostrophe", lazy.layout.next()), desc="Move window focus to other window"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

    # MonadTall layout
    Key([mod], "m", lazy.layout.maximize(),
        desc="Grow window to the left"),
    Key([mod, "control"], "space", lazy.layout.flip(),
        desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.shrink(),
        desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow(),
        desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(),
        desc="Reset all window sizes"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes

   # Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
       # desc="Toggle between split and unsplit sides of stack"),
    Key([mod], "f", lazy.window.toggle_fullscreen(), desc='toggle floating'),
    Key([mod, "shift"], "f", lazy.window.toggle_floating(), desc='toggle floating'),
    Key([mod], "Return", lazy.spawn(myTerm), desc="Launch myTerm"),
    Key([mod], "d", lazy.spawn("dmenu_run"), desc="Launch dmenu"),
    Key([mod, "shift"], "Return", lazy.spawn("dmenu_run"), desc="Launch dmenu"),


    # Toggle between different layouts as defined below
    Key([mod], "space", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "shift"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "shift"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawncmd(),
        desc="Spawn a command using a prompt widget"),

    # Alst keys
    Key([alt], "j", lazy.spawn("amixer set Master 5%-"), desc="Descrease volume"),
    Key([alt], "k", lazy.spawn("amixer set Master 5%+"), desc="Inscrease volume"),
    Key([alt], "m", lazy.spawn("amixer set Master toggle"), desc="Mute"),
    Key([alt], "apostrophe", lazy.spawn("./.bin/invidious"), desc="Run invidious script"),

    # F<key-pad> 
    Key([mod], "w", lazy.spawn(myBrowser), desc="Open browser"),
    Key([mod], "F2", lazy.spawn(myTerm+" -e vifm"), desc="Open vifm"),
    Key([mod, "shift"], "F2", lazy.spawn(myTerm+" -e sudo vifm"), desc="Open vifm"),
     Key([mod, "shift"], "F3", lazy.spawn("emacsclient -c -a 'emacs'"), desc="Lauch emacs client"),
    Key([mod], "F3", lazy.spawn(myTerm+" -e nvim"), desc="Lauch emacs client"),

    # Scripts from ~/.bin
    KeyChord([mod], "a", [
        Key([], "a",
        lazy.spawn("./.bin/binscripts")),
        Key([], "w",
        lazy.spawn("./.bin/random-wallpaper")),
    ]),

    # Dmenu scripts fot edit config files
    KeyChord([mod], "s", [
        Key([], "s",
        lazy.spawn("./.dmscripts/dmscripts")),
        Key([], "a",
        lazy.spawn("./.dmscripts/dmappimage")),
        Key([], "c",
        lazy.spawn("./.dmscripts/dmconf")),
        Key([], "q",
        lazy.spawn("./.dmscripts/dmquickmarks")),
        Key([], "b",
        lazy.spawn("./.dmscripts/dmbookmarks")),
        Key([], "w",
        lazy.spawn("./.dmscripts/dmsearch")),
    ]),

    # Shutdown and reboot the system
    KeyChord([mod], "0", [
        Key([], "s",
        lazy.spawn("shutdown now")),
        Key([], "r",
        lazy.spawn("reboot")),
    ]),
]

groups = [Group(i) for i in "1234"]

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen(),
            desc="Switch to group {}".format(i.name)),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True),
            desc="Switch to & move focused window to group {}".format(i.name)),
        # Or, use below if you prefer not to switch to that group.
        # # mod1 + shift + letter of group = move focused window to group
        # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
        #     desc="move focused window to group {}".format(i.name)),
    ])

###LAYOUTS###
layouts = [
    layout.MonadTall(border_focus='#ADD8E6', border_width=3, margin=4),
    layout.Max(),
    # layout.Columns(border_focus_stack='#c215ff'),
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    layout.Zoomy(),
    # layout.Floating(),
]

###COLORS###
colors = [["#282c34", "#282c34"], # panel background
          ["#3d3f4b", "#434758"], # background for current screen tab
          ["#ffffff", "#ffffff"], # font color for group names
          ["#ff5555", "#ff5555"], # border line color for current tab
          ["#74438f", "#74438f"], # border line color for 'other tabs' and color for 'odd widgets'
          ["#4f76c7", "#4f76c7"], # color for the 'even widgets'
          ["#e1acff", "#e1acff"], # window name
          ["#ecbbfb", "#ecbbfb"], # backbround for inactive screens
          ["#ADD8E6", "#ADD8E6"]] # backbround for active screens

widget_defaults = dict(
    font='Hack',
    fontsize=17,
    padding=3,
    background=colors[0]
)
extension_defaults = widget_defaults.copy()

###BAR###
screens = [
    Screen(
        top=bar.Bar(
            [
                widget.Image(
                       filename = "~/Downloads/icons/python.png",
                       scale = "False",
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn("./.bin/invidious")}
                       ),
                widget.CurrentLayout(),
                widget.GroupBox(),
                widget.Prompt(),
                widget.WindowName(),
                widget.Chord(
                    chords_colors={
                        'launch': ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                widget.Sep(
                       linewidth = 1,
                       padding = 20,
                       foreground = colors[0],
                       background = colors[0]
                ),
                 widget.Image(
                       filename = "~/Downloads/icons/cpu.png",
                       scale = "False",
                       mouse_callbacks = {}
                       ),
                widget.CPU(
                foreground = colors[8]
                ),
                widget.Sep(
                       linewidth = 1,
                       padding = 20,
                       foreground = colors[2],
                       background = colors[0]
                ),
                widget.Image(
                       filename = "~/Downloads/icons/ram.png",
                       scale = "False",
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm)}
                       ),
                widget.Memory(
                foreground = colors[8],
                mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e htop')},
                ),
                widget.Sep(
                       linewidth = 1,
                       padding = 20,
                       foreground = colors[2],
                       background = colors[0]
                ),
                widget.Image(
                       filename = "~/Downloads/icons/sound.png",
                       scale = "False",
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn("amixer set Master toggle")}
                       ),
                widget.Volume(
                foreground = colors[8],
                ),
                widget.Sep(
                       linewidth = 1,
                       padding = 20,
                       foreground = colors[2],
                       background = colors[0]
                ),
                widget.Image(
                       filename = "~/Downloads/icons/calendar.png",
                       scale = "False",
                       mouse_callbacks = {}
                       ),
                widget.Clock(
                foreground = colors[8],
                format='%a %d-%m %H:%M'),
                widget.Wlan(),
                widget.Systray(),
                # widget.QuickExit(default_text='[Quit]'),
            ],
            24,
        ),
        bottom=bar.Bar(
            [
            ],
            34,
        )
    ),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
    Match(title='nitrogen'),  # Sets wallpaper 
    Match(title='gcolor2'),
    Match(title='galculator'),
    Match(title='VirtualBox Manager'),
])

auto_fullscreen = False
focus_on_window_activation = "smart"

mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

# @hook.subscribe.startup_once
# def start_once():
    # home = os.path.expanduser('~')
    # subprocess.call([home + '/.config/qtile/autostart.sh'])

# @hook.subscribe.startup_once
# def autostart():
#     processes = [
#          [ 'nitrogen', '--restore' ],
#         # [ '~/.fehbg' ],
#          [ 'setxkbmap', '-model', 'abnt2', '-layout', 'br', '-variant', 'abnt2' ],
#          [ 'xclip' ],
#          [ 'nm-applet' ],
#     ]

#     for p in processes:
#         subprocess.Popen(p)

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "Qtile"
