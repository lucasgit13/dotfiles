"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Plugins 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
call plug#begin()

" Plug 'vim-airline/vim-airline'
Plug 'itchyny/lightline.vim'
Plug 'tpope/vim-fugitive'
Plug 'ap/vim-css-color'
Plug 'vifm/vifm.vim'
Plug 'ryanoasis/vim-devicons'
Plug 'gorodinskiy/vim-coloresque'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'vim-python/python-syntax'
Plug 'vimwiki/vimwiki'
Plug 'morhetz/gruvbox'
Plug 'tpope/vim-commentary'

call plug#end()
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Leader key
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let mapleader=" "
nnoremap <SPACE> <Nop>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Remap Keys
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Remap F3 to set "noh"
nnoremap <ESC> :noh<CR>
" Map F5 to save the current file
map <F5> :w<CR>
" Map F10 to save and quit from the file
map <F10> :wq<CR>

" Create a keybind in a specific filetype "need to put the line below the ftdetect of the plugin
" map <buffer> ,m :make build<CR> 

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" NERDtree remap keys
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nnoremap <C-n> :NERDTreeToggle<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:python_highlight_all = 1

" Always show statusline
set laststatus=2
" Remove the standard statusline
set noshowmode

""""""""""""""""""""""""""""""""""""""
" Gruvbox settings "
let g:gruvbox_contrast_dark = 'medium'
""""""""""""""""""""""""""""""""""""""
colorscheme gruvbox 

" The lightline.vim theme
 let g:lightline = {
      \ 'colorscheme': 'seoul256',
            \ }

set runtimepath+=expand('~/.vim/vim-mql4')
set nocompatible
filetype plugin on
set number relativenumber
set wildmenu		        	" Display all matches when tab complete.
set incsearch                   " Incremental search
set nobackup                    " No auto backups
set noswapfile                  " No swap
set expandtab                   " Use spaces instead of tabs.
set smarttab                    " Be smart using tabs ;)
set smartindent
set cindent
set tabstop=4
set shiftwidth=4                " One tab == four spaces.
set tabstop=4                   " One tab == four spaces.
set softtabstop=4
set path+=**                    " Searches current directory recursively.
set paste
set t_Co=256                    " Set if term supports 256 colors.
set termguicolors
set clipboard+=unnamedplus
set mousemodel=extend
set scrolloff=10
set mouse=a
syntax enable

" Set up font Neovide
set guifont=Hack:h15

"""""""""""""""""""""""""""""""""""""
"Splits 
"""""""""""""""""""""""""""""""""""""
set splitbelow splitright

" Remap splits navigation with Control + hjkl
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Set Space + wc to close focus split
map <Leader>wc <C-w>c

" Make adjusing split sizes a bit more friendly
noremap <silent> <C-Left> :vertical resize +3<CR>
noremap <silent> <C-Right> :vertical resize -3<CR>
noremap <silent> <C-Up> :resize +3<CR>
noremap <silent> <C-Down> :resize -3<CR>

" Open a terminal in vertical split
map <Leader>tt :vnew term://zsh<CR>

" Change 2 split windows from vert to horiz or horiz to vert
map <Leader>th <C-w>t<C-w>H
map <Leader>tk <C-w>t<C-w>K

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Vifm
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
map <Leader>vv :Vifm<CR>
map <Leader>vs :VsplitVifm<CR>
map <Leader>sp :SplitVifm<CR>
map <Leader>dv :DiffVifm<CR>
map <Leader>tv :TabVifm<CR>

